<?php
session_start();
include('config.php');
if (isset($_SESSION['login_user'])) {
//            echo "Welcome " . $_SESSION['login_user'];
} else {
    header("location:  login.php");
}
?>
<html lang="en">
    <h1>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content="">
            <meta name="author" content="">
            <link rel="icon" href="../../favicon.ico">
            <title>New Task Add | Task Manager</title>
            <!-- Bootstrap core CSS -->
            <link href="css/bootstrap.min.css" rel="stylesheet">
            <!-- Custom styles for this template -->
            <link href="css/welcomePage.css" rel="stylesheet">
<!--            <script type="text/javascript" src="js/functions.js"></script>-->
            <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
            <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->


    </h1> 

</head>

<body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse"></button>
                <a class="navbar-brand" href="welcome.php"><?php echo $_SESSION['login_user']; ?></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="welcome.php">TaskList </a></li>
                    <li class="active"><a href="">Add New</a></li>
                </ul>
                <a class="nav navbar-nav navbar-brand navbar-right" href="logout.php">Logout</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="panel  panel-success">
            <!-- Default panel contents -->
            <div class="panel-heading"><strong>Add New Task</strong></div>
            <form action="Welcome.php?prop_id=Tasks Successfully Added" name="loginForm" class="form-signin" role="form" action="" method="post" onsubmit="return validateTask()" onchange="return validateFileSize()" enctype="multipart/form-data">
                <div id="task-wraper">
                <!--    <label>Task Name :</label>-->
                <div class="form-group">
                    <input type="text" name="taskName"  class="form-control" placeholder="Task Name"  /><br />
                </div>
                <!--    <label>Task Description :</label>-->
                <div class="form-group">
    <!--                <textarea class="form-control" rows="10"></textarea>-->
                    <input type="text" name="description" class="form-control" placeholder="Task Description" /><br />  
                </div>
                <div class="form-group">
                    <label>Select A File To Upload:</label>
                    <input type="file" name="uploadedFile[]"  id="myFile"/><br />
                </div>
                </div>
                <div class="form-group">
                    <button  type="button" id="add-more" class="btn btn-lg btn-info btn-block" >Add More Files</button>
                    <button type="submit" name='submit' class="btn btn-lg btn-success btn-block"  >Save</button>
                </div>
        </div>
    </div>

</form>
<script>
    $("#add-more").click(function(){
        var elements = '<div class="form-group"><input type="file" name="uploadedFile[]" id="add-more"/><br /></div>';
        $("#task-wraper").append(elements);
        // remove 
        $(".remove").click(function(){
            this.parentNode.parentNode.removeChild(this.parentNode);
        })
    })
</script>
</body>
</div>

</html>