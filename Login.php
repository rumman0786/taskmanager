<?php
include("config.php");
session_start();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
// username and password sent from Form
    $useremail = mysql_real_escape_string($_POST['useremail']);
    $mypassword = mysql_real_escape_string($_POST['password']);

    $saltQuery = "SELECT `salt` FROM `userlist` WHERE `email` = '" . $useremail . "'";
    $resultSalt = mysql_query($saltQuery);
    $rowSalt = mysql_fetch_assoc($resultSalt);
    $salt = $rowSalt['salt'];
    $saltedPW = $mypassword . $salt;
    $hashedPW = hash('sha256', $saltedPW);

    $loginQuery = "select * from `userlist` where `email` = '$useremail' and password = '$hashedPW'";
    $resultLogin = mysql_query($loginQuery);
    $rowUser = mysql_fetch_array($resultLogin);
    $count = mysql_num_rows($resultLogin);

    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    } else {
        echo "Connected to MySQL: ";
    }

    if ((false === $resultSalt) || (false === $resultLogin)) {
        echo mysql_error();
    } else {
        echo "Welcome" . $rowUser['name'] . "</br>";
        echo "You have " . $count . "data";
    }
// If result matched $myusername and $mypassword, table row must be 1 row
    if ($count == 1) {
        //while ($row = mysql_fetch_array($result)) {
        //}
        echo $_SESSION['userId'] = $rowUser['id'];
        echo $_SESSION['login_user'] = $rowUser['name'];
        header("location: welcome.php");
    } else {
        $error = "Your Login Name or Password is invalid";
        echo $error;
    }
}
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="images/favicon.ico">
        <title>Login</title>
        <script src="js/respond.js"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/signin.css">
    </head>

    <body>

        <!-- javascript -->
<!--        <script src="http://code.jquery.com/jquery-latest.min.js"></script>-->
        <script src="js/bootstrap.min.js"></script>
        <div class="container">
            <h2 class="form-signin-heading">Please sign in</h2>
            <form class="form-signin" role="form" action="" method="post">
                <!--            <label>UserName :</label>-->
                <input type="text" name="useremail" class="form-control" placeholder="Email address" required autofocus/><br />
                <input type="password" name="password" class="form-control" placeholder="Password"/><br />
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                <p class="signup-helper">
                    New to Task Manager?
                    <a id="login-signup-link" href="http://localhost/phpproject/register.php">Sign up now »</a>
                </p>
            </form>
        </div>
    </body>
</html>


