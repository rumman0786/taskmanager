<?php
session_start();
include('config.php');
if (isset($_SESSION['login_user'])) {
//            echo "Welcome " . $_SESSION['login_user'];
} else {
    header("location:  login.php");
}
?>
<html lang="en">
    <h1>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content="">
            <meta name="author" content="">
            <link rel="icon" href="../../favicon.ico">
            <title>Content Not Found | Task Manager</title>
            <!-- Bootstrap core CSS -->
            <link href="css/bootstrap.min.css" rel="stylesheet">
            <!-- Custom styles for this template -->
            <link href="css/welcomePage.css" rel="stylesheet">
            <script type="text/javascript" src="js/functions.js"></script>
            <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
            <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->


    </h1> 

</head>

<body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse"></button>
                <a class="navbar-brand" href="welcome.php"><?php echo $_SESSION['login_user']; ?></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="welcome.php">TaskList </a></li>
                    <li><a href="addnew.php">Add New</a></li>
                </ul>
                <a class="nav navbar-nav navbar-brand navbar-right" href="logout.php">Logout</a>
            </div>
        </div>
    </div>
    
    
    <div class="container">
        <div class="panel panel-default" align="center">
            <div class="panel-heading">
                <h3 class="panel-title">Sorry, this page isn't available</h3>
            </div>
            <div class="panel-body" >
                <p>The link you followed may be broken, or the page may have been removed.</p><br/>
                <img src ='images/brokenlink_img.png' >
                
            </div>
            <div class="panel-footer">
                <p class="signup-helper">
                    New to Task Manager?
                    <a id="login-signup-link" href="http://localhost/phpproject/register.php">Sign up now »</a>
                </p>
            </div>
            
        </div>
    </div>



</body>