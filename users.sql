-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 02, 2014 at 11:26 AM
-- Server version: 5.5.20
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `users`
--

-- --------------------------------------------------------

--
-- Table structure for table `tasklist`
--

CREATE TABLE IF NOT EXISTS `tasklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskName` text NOT NULL,
  `taskDescription` text NOT NULL,
  `userId` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=305 ;

--
-- Dumping data for table `tasklist`
--

INSERT INTO `tasklist` (`id`, `taskName`, `taskDescription`, `userId`) VALUES
(303, '1', '1', '6'),
(304, 'Rocko', 'Done', '6');

-- --------------------------------------------------------

--
-- Table structure for table `task_file_map`
--

CREATE TABLE IF NOT EXISTS `task_file_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `location` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `task_id` (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=220 ;

--
-- Dumping data for table `task_file_map`
--

INSERT INTO `task_file_map` (`id`, `task_id`, `location`) VALUES
(217, 303, 'storedData/1.jpg'),
(218, 304, 'storedData/2.jpg'),
(219, 304, 'storedData/3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `userlist`
--

CREATE TABLE IF NOT EXISTS `userlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `userlist`
--

INSERT INTO `userlist` (`id`, `name`, `email`, `password`, `salt`) VALUES
(5, 'rusho', 'rusho@rusho.com', '3a061bd749d83e7b2ebfb1f8413a78a18df15410fcac494897cb61ce59174cc4', 'ced6da637653675faa59b469b150c8a854222cc4da34e6e60d7081b6323d035c'),
(6, 'Rumman', 'rumman.ashraf@gmail.com', '47c1c25039212af338b9d5abcf9a1c953e38fc7d84e2a5f761a51c43f9cc7d5a', '52f73676b843475f46a4540384c2a4c679c60094ad59afd32a9f7a6d7544a8c7'),
(8, 'Swapon Ahamed', 'swapon@gmail.com', 'b87e55c583f9dbf7d1816e27657f64528f9b4a392808538fa06087dd8672c0e8', '5cd7a3f174abaa369b1c547cda526aacb8f5342e3f2f78e8f4a58045e181152b'),
(10, 'Aktar', 'swapon@gmail.com', '0b544ede63b0e72d8549748e57b882e2a3e0ed3d6e5973b6aa96d172a8516e3a', '61d5758ad9625217dd78eeb0b47f537f2860072c7d8312dd7fcefb3e75acce73');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `task_file_map`
--
ALTER TABLE `task_file_map`
  ADD CONSTRAINT `task_file_map_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasklist` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
