<?php
session_start();
include('config.php');
if (isset($_SESSION['login_user'])) {
//            echo "Welcome " . $_SESSION['login_user'];
} else {
    header("location:  login.php");
}
?>
<html lang="en">
    <h1>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content="">
            <meta name="author" content="">
            <link rel="icon" href="../../favicon.ico">
            <title>Task Manager</title>
            <!-- Bootstrap core CSS -->
            <link href="css/bootstrap.min.css" rel="stylesheet">
            <!-- Custom styles for this template -->
            <link href="css/welcomePage.css" rel="stylesheet">
            <script type="text/javascript" src="js/functions.js"></script>
            <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
            <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->


    </h1> 

</head>

<body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse"></button>
                <a class="navbar-brand" href="welcome.php"><?php echo $_SESSION['login_user']; ?></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="">TaskList </a></li>
                    <li><a href="addnew.php">Add New</a></li>
                </ul>
                <a class="nav navbar-nav navbar-brand navbar-right" href="logout.php">Logout</a>
            </div>
        </div>
    </div>

    <?php
    if (isset($_GET['prop_id'])) {
        ?>
        <div class="container">
                <!-- Default panel contents -->
                <div class="panel panel-default" align="center">
                    <div class="panel-heading">
                        <h3 class="panel-title">Confirmation Message</h3>
                    </div>
                    <div class="panel-body" >
                        <p><?php echo $_GET['prop_id']; ?><br/>
                    </div>

                </div>
        </div>

        <?php
    }
    ?>

    <div id="addNewButton" class="container"><div class="pull-right"><button onclick="location.href = 'addnew.php';"type="button" class="btn btn-primary ">Add New</button></div></div>
    <div class="table-responsive container">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><strong>Task List</strong></div>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Task Name</th>
                        <th>Task Description</th>
                        <th colspan="3"> Functionality </th>
                    </tr>
                </thead>
                <tbody
                <?php
                $sql_task = " SELECT tl.id, tl.taskName, tl.taskDescription, tm.location
                         FROM tasklist tl
                         LEFT JOIN userlist u ON u.id = tl.userId
                         LEFT JOIN task_file_map tm ON tl.id = tm.task_id
                         WHERE u.id =" . $_SESSION['userId'] . "
                         GROUP BY tl.id";

                $result = mysql_query($sql_task);

                if (false === $result) {
                    echo mysql_error();
                } else {
                    while ($row = mysql_fetch_array($result)) {
                        $tabletTaskName = $row['taskName'];
                        $tabletTaskDescription = $row['taskDescription'];


                        if (strlen($tabletTaskDescription) > 100) {
                            $tabletTaskDescription = substr($tabletTaskDescription, 0, 100) . " ... ";
                        }
                        ?>
                            <tr>
                                <td><p>  <?php echo $tabletTaskName; ?> </p></td>
                                <td><p id="div2"> <?php echo $tabletTaskDescription; ?> </p></td>
                                <?php
                                echo "<td><a href =\"delete.php?prop_id=" . $row['id'] . "\">"
                                ?><span class="glyphicon glyphicon-trash"></span> Delete</a></td>
                                <?php
                                echo "<td><a href =\"editPage.php?prop_id=" . $row['id'] . "\">"
                                ?>  <span class="glyphicon glyphicon-edit"></span> Edit <?php "</a></td>"; ?>
                        <?php
                        $sql_pics = "SELECT location
                          FROM task_file_map
                          WHERE task_id =" . $row['id'];
                        $result_loc = mysql_query($sql_pics);
                        echo "<td>";
                        while ($loc = mysql_fetch_array($result_loc)) {
                            ?>
                            <img src = '<?php echo $loc['location'] ?>' height = '100px' width = '100px'>
                            <?php
                        }
                        echo "</td>";
                        ?><!-- class=img-responsive img-circle> -->


                        </tr>
                        <?php
                    }
                }
                ?>   
                </tbody>
            </table>
        </div>
    </div>
</div>


</body>
<?php
include("config.php");
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
} else {
//    echo "Connected to MySQL: ";
}

if (isset($_POST) && !empty($_POST)) {

//    var_dump($_FILES);


// username and password sent from Form
    $taskName = mysql_real_escape_string($_POST['taskName']);
    $taskDescription = mysql_real_escape_string($_POST['description']);
    $userId = $_SESSION['userId'];


//Insert
    if ($_POST['submit'] == 0) {
        $sql_taskList = "INSERT INTO `tasklist`(`taskName`, `taskDescription`, `userId`) VALUES ('$taskName', '$taskDescription', '$userId')";
        mysql_query($sql_taskList);
//        echo "Count: " . $count = count($_FILES['uploadedFile']['tmp_name']) . "<br>";
        $task_id = mysql_insert_id();
        $msg = array();
//        echo $task_id;
        foreach ($_FILES['uploadedFile']['tmp_name'] as $index => $tmpName) {
            if (!empty($_FILES['uploadedFile']['error'][$index])) {
                // some error occured with the file in index $index
                // yield an error here
                echo $msg[] = "Error:::::" . $_FILES['uploadedFile']['error'][$index];
                continue; // return false also immediately perhaps??
            }
            $filePath = "storedData/" . $_FILES['uploadedFile']['name'][$index];//."_".$userId;
            $sql = "INSERT INTO `task_file_map`(`task_id`, `location`) VALUES ('$task_id', '$filePath')";
            mysql_query($sql);
            move_uploaded_file($tmpName, $filePath);
            $msg[] = "Data Added";
//            continue;
//            move_uploaded_file( $tmpName, $someDestinationPath ); // move to new location perhaps?
        }
    }
//Delete
//    else if ($_POST['submit'] == 1) {
//        $sql = "DELETE FROM `tasklist` WHERE taskName = '$taskName'";
//        $msg = "Data Deleted";
//    }
//Retrieve
//    else if ($_POST['submit'] == 2) {
//        $sql = "SELECT * FROM `tasklist` WHERE taskName = '$taskName'";
//        $result = mysql_query($sql);
//        while ($row = mysql_fetch_array($result)) {
//            echo $row['taskName'] . " : " . $row['taskDescription'];
//        }
//        $msg = "Successfully Shown";
//    }
//Update    
//    else if ($_POST['submit'] == 3) {
//        $sql = "UPDATE `tasklist` SET `taskDescription` = '$taskDescription' WHERE `taskName` = '$taskName'";
//        $msg = "Edited";
//    }
    else if ($_POST['submit'] == 4) {
        header("location: logout.php");
    }


//    $result = mysql_query($sql);
    if (false === $result) {
        echo mysql_error();
    } else {
//        print_r($msg);
        // header("location: welcome.php");
    }
}
?>

<!--<div class="container">
    <h2 class="form-signin-heading">Add New Tasks</h2>
    <form  name="loginForm" class="form-signin" role="form" action="" method="post" onsubmit="return validateTask()" onchange="return validateFileSize()" enctype="multipart/form-data">
            <label>Task Name :</label>
        <div class="form-group">
            <input type="text" name="taskName"  class="form-control" placeholder="Task Name"  /><br />
        </div>
            <label>Task Description :</label>
        <div class="form-group">
            <input type="text" name="description" class="form-control" placeholder="Task Description" /><br />
        </div>
        <div class="form-group">
            <label>Select A File To Upload:</label>
            <input type="file" name="uploadedFile" id="myFile"/><br />
        </div>
        <div class="form-group">
            <button name='submit' class="btn btn-lg btn-primary btn-block" type="submit" value='0'>Add</button>

    </form>
</div>-->

</html>

