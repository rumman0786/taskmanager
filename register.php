<?php
include('config.php');
?>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="images/favicon.ico">
        <title>Register</title>
        <script src="js/respond.js"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/signin.css">
    </head>

    <body>
        <script src="js/bootstrap.min.js"></script>
        <div class="container">
            <div class="panel panel-default">
                <h2 class="form-signin-heading">Register</h2>
                <form class="form-signin" role="form" action="" method="post">
                    <input type="text" name="username" class="form-control" id="name" placeholder="Name" onkeyup="checkCredentials()" required autofocus/><br />
                    <span id="nameMessage" class="confirmName"></span>
                    <input type="text" name="useremail" id="email" class="form-control" placeholder="Email address" onkeyup="checkCredentials()" /><br />
                    <span id="emailMessage" class="confirmEmail"></span>
                    <input type="password" name="password" id="pass1" class="form-control"  placeholder="Password" onkeyup="checkCredentials()"/><br />
                    <input type="password" name="confirmPassword" id="pass2" class="form-control" placeholder="Confirm Password" onkeyup="checkCredentials()"/><br />
                    <span id="confirmMessage" class="confirmPassword"></span>
                    <button class="btn btn-lg btn-success btn-block" disabled="disabled" id="registerButton" type="submit">Register</button>
                </form>
            </div>
        </div>
        <script src="js/jquery-2.1.1.min.js"></script>
        <script src="js/register.js"></script>
    </body>

</html>


<?php
if (isset($_POST) && !empty($_POST)) {
//    var_dump($_POST);
    $userName = mysql_real_escape_string($_POST['username']);
    $userEmail = mysql_real_escape_string($_POST['useremail']);
    $userPassword = mysql_real_escape_string($_POST['password']);
    $salt = bin2hex(mcrypt_create_iv(32, MCRYPT_RAND));
    $saltedPW = $userPassword . $salt;
    $hashedPW = hash('sha256', $saltedPW);
    $emailValid = true;
    $nameValid = true;
    if (!filter_var($userEmail, FILTER_VALIDATE_EMAIL)) {
        $emailValid = false;
        echo "not an email";
    }
    if (!preg_match("/^[a-zA-Z ]+$/", $userName)) {
        $nameValid = false;
    }

    if (isset($userName) && isset($userEmail) && isset($userPassword) && $emailValid && $nameValid) {
        $sql = "INSERT INTO `userlist`(`name`, `email`, `password`,`salt`) VALUES ('" . $userName . "','" . $userEmail . "','" . $hashedPW . "','" . $salt . "')";
        $result = mysql_query($sql);
        if (false === $result) {
            echo mysql_error();
        } else {
            echo "Registration Successful :) </br>";
        }
    }
}
?>