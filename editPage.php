<?php
include('config.php');
session_start();
if (isset($_SESSION['login_user'])) {
//            echo "Welcome " . $_SESSION['login_user'];
} else {
    header("location:  login.php");
}
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <title>Edit Tasks</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/editPage.css" rel="stylesheet">


        <?php
        if (!(isset($_GET['prop_id']))) {
            header("location: error_page.php");
        }
        $id = $_GET['prop_id'];
        $sql = "SELECT * FROM `tasklist` WHERE id = " . $id;
        $query = mysql_query($sql);
        $row = mysql_fetch_array($query);

        if (false === $query) {
            echo "INIT::" . mysql_error();
        }

        $taskName = $row['taskName'];
        $taskDescription = $row['taskDescription'];
//        $taskFile = substr($row['location'], 11);
        ?>
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">

                    </button>
                    <a class="navbar-brand" href="welcome.php"><?php echo $_SESSION['login_user']; ?></a>
                </div>
                <div class="navbar-collapse collapse">
                    <a class="nav navbar-nav navbar-brand navbar-right" href="logout.php">Logout</a>
                </div>
            </div>
        </div>
        <div class="container">
            <h2 class="form-signin-heading">Make Changes</h2>
            <form action="" method="post" class="form-signin" role="form" enctype="multipart/form-data">
                <div id="task-wraper">
                    <div class="form-group">
                        <label class="sr-only" >Task Name :</label>
                        <input type="text" name="taskName" class="form-control" value="<?php echo htmlspecialchars($taskName, ENT_QUOTES); ?>" /><br />
                    </div>
                    <div class="form-group">
                        <label  class="sr-only" >Task Description :</label>
                        <input type="text" name="description" class="form-control"  value="<?php echo htmlspecialchars($taskDescription, ENT_QUOTES); ?>" /><br />
                    </div>
                     <div class="form-group" align="center">
                        <?php
                        $sql_image = "SELECT  * FROM `task_file_map` WHERE task_id = " . $id;
                        $image_query = mysql_query($sql_image);
                        $max_img_in_a_row = 3 ;
                        $i = 1 ;
                        while ($loc = mysql_fetch_assoc($image_query)) {
                            ?>
                            <span>
                                <a href="<?php echo "download.php?location=" . $loc['location']; ?>"><img src = '<?php echo $loc['location'] ?>' height = '100px' width = '100px'></a>
                                <span class="glyphicon remove glyphicon-trash" onclick="delete_image(<?php echo $loc['id']; ?>,<?php echo "'" . $loc['location'] . "'"; ?>)" id="img-<?php echo $loc['id'] ?>"></span> 
                            </span>
                            <?php
                            if($i % $max_img_in_a_row == 0){
                                echo "<br/>";
                            }
                            $i++ ;
                        }
                        ?>
<!--                         <button class="btn btn-lg btn-success custom" type="button" id="add-more" name='submit'>Add More Images</button>-->
<!--                    <div align="center" class="container"><button class="btn btn-lg btn-success custom" type="button" id="add-more" name='submit'>Add More Images</button></div>-->
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-lg btn-success btn-block" type="button" id="add-more" name='submit'>Add More Images</button>
                    <button class="btn btn-lg btn-success btn-block" type="submit" name='submit'>Update</button>
                </div>
            </form>
           

            
        </div>
        <script>
            function delete_image(id,location) {
                //                alert(location);
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp=new XMLHttpRequest();
                } else { // code for IE6, IE5
                    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                }
                if ( (id!="") && (location !="")) {
                    xmlhttp.open("GET","editPage.php?id="+id+"&location="+location,true);
                    xmlhttp.send();
                } 
                xmlhttp.onreadystatechange=function(response) {
                    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                        window.location.href= document.URL;
                        //                        alert(html);
                        
                    }
                }
            }
            
            function download_image(location) {
                //                alert(location);
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp=new XMLHttpRequest();
                } else { // code for IE6, IE5
                    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                }
                if ( (id!="") && (location !="")) {
                    xmlhttp.open("GET","download.php?id="+id+"&location="+location,true);
                    xmlhttp.send();
                } 
                xmlhttp.onreadystatechange=function(response) {
                    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                        //                        window.location.href= document.URL;
                        //                        alert(html);
                        
                    }
                }
            }
            $("#add-more").click(function(){
                var elements = '<div class="form-group"><label>Task File:</label><input type="file" name="uploadedFile[]" id="add-more"/><br /></div>';
                $("#task-wraper").append(elements);
                // remove 
                $(".remove").click(function(){
                    this.parentNode.parentNode.removeChild(this.parentNode);
                })
            })
            
        </script>
    </body>


    <?php
    if (isset($_POST) && !empty($_POST)) {
//        var_dump($_POST);
//        var_dump($_FILES);
        $taskName = mysql_real_escape_string($_POST['taskName']);
        $taskDescription = mysql_real_escape_string($_POST['description']);
        $userId = mysql_real_escape_string($_SESSION['userId']);
        $msg = array();

        //updating the task name and description
        $sql_taskList = "UPDATE `tasklist` SET `taskName`='" . $taskName . "' , `taskDescription`='" . $taskDescription . "' WHERE `userId` =" . $userId;
        update($sql_taskList);
        //getting the insert id
        $task_id = mysql_insert_id();

        //checking if files uploaded and their count
        if (empty($_FILES['uploadedFile']['tmp_name'])) {
            $msg[] = "No Files";
            $count = 0;
            header("location: Welcome.php?prop_id=Task Title & Description Changed");
        } else {
            $count = count($_FILES['uploadedFile']['tmp_name']);
        }
        echo "COUNT:: " . $count . "<br>";
        foreach ($_FILES['uploadedFile']['tmp_name'] as $index => $tmpName) {
            if (!empty($_FILES['uploadedFile']['error'][$index])) {
                // some error occured with the file in index $index
                // yield an error here
                $msg[] = "Error:::::" . $_FILES['uploadedFile']['error'][$index];
                continue; // return false also immediately perhaps??
            }
            $filePath = "storedData/" . $_FILES['uploadedFile']['name'][$index];
            $sql = "INSERT INTO `task_file_map`(`task_id`, `location`) VALUES ('$id', '$filePath')";
            $result = mysql_query($sql);
            if (false === $result) {
                echo "UPDATE::" . mysql_error() . "<br/>";
                continue;
                $msg[] = "Data Not Added";
            }
            move_uploaded_file($tmpName, $filePath);
            $msg[] = "Data Added";
//            continue;
//            move_uploaded_file( $tmpName, $someDestinationPath ); // move to new location perhaps?
        }

//        
//        die();
//        if (is_uploaded_file($_FILES["uploadedFile"]["error"])) {//Error in this line for multiple files
//            var_dump($_FILES["uploadedFile"]["error"] );
//            echo implode("=>", $_FILES["uploadedFile"]["error"]);
//            die();
//            
//        } else {
////            $sql_taskList = "UPDATE `tasklist` SET `taskName`='" . $taskName . "' , `taskDescription`='" . $taskDescription . "' WHERE `userId` =" . $userId;
////            update($sql_taskList);
//            $task_id = mysql_insert_id();
//            foreach ($_FILES['uploadedFile']['tmp_name'] as $index => $tmpName) {
//                if (!empty($_FILES['uploadedFile']['error'][$index])) {
//                    // some error occured with the file in index $index
//                    // yield an error here
//                    echo $msg[] = "Error:::::" . $_FILES['uploadedFile']['error'][$index];
//                    continue; // return false also immediately perhaps??
//                }
//                $filePath = "storedData/" . $_FILES['uploadedFile']['name'][$index];
//                $sql = "INSERT INTO `task_file_map`(`task_id`, `location`) VALUES ('$task_id', '$filePath')";
//                mysql_query($sql);
//                move_uploaded_file($tmpName, $filePath);
//                $msg[] = "Data Added";
//            continue;
//            move_uploaded_file( $tmpName, $someDestinationPath ); // move to new location perhaps?
//            }
//        }
        implode(",", $msg);
//        else {
//            $name = $_FILES["uploadedFile"]["name"];
//            $tmp_path = $_FILES["uploadedFile"]["tmp_name"];
//            echo $filePath = mysql_real_escape_string("storedData/" . $name);
//            if ($taskFile !== $name) {
//                echo "In IF statement";
//                move_uploaded_file($tmp_path, $filePath);
//                $sql = "UPDATE `tasklist` SET `taskDescription`= '" . $taskDescription . "' , `taskName` = '" . $taskName . "' , `location` = '" . $filePath . "' , userId = " . $userId . " WHERE id = " . $id;
//                delete();
//                update($sql);
//            } else {
//                echo "In ELSE statement";
//                $sql = "UPDATE `tasklist` SET `taskDescription`= '" . $taskDescription . "' , `taskName` = '" . $taskName . "' , userId = " . $userId . " WHERE id = " . $id;
//                update($sql);
//            }
//        }
//        $sql_taskList = "UPDATE `tasklist` SET `taskName`='".$taskName."', SET `taskDescription`='".$taskDescription."' WHERE `userId` =".$userId;
        //        $sql = "UPDATE `tasklist` SET `taskDescription` = '$taskDescription' WHERE `taskName` = '$taskName'";
//        mysql_query($sql_taskList);
//        header("location: editPage.php?prop_id=" . $id);
        header("location: Welcome.php?prop_id=Task Title,Description modified and files added");
    }

    function delete() {
        $id = $_GET['prop_id'];
        $fileGetSql = "SELECT * FROM `tasklist` WHERE id = '$id'";
        $resultFileNameResult = mysql_query($fileGetSql);
        $row = mysql_fetch_array($resultFileNameResult);
//$totalLocation stores the total path including the file.
        $totalLocation = "C:/wamp/www/phpproject/" . $row['location'];
//$location strips the total location to get the current location of the file.
        $location = substr($totalLocation, 0, 33);
//$fileName stores the name of the file
        echo "<br/> Delete the file :: " . $fileName = substr($totalLocation, 34);
        if (false === $resultFileNameResult) {
            echo mysql_error();
        }
        $old = getcwd(); // Save the current directory
        chdir($location);
        unlink($fileName);
        chdir($old); // Restore the old working directory    
    }

    function update($query) {
        $result = mysql_query($query);
        if (false === $result) {
            echo "UPDATE::" . mysql_error();
        }
    }

    if (isset($_GET['id'])) { //&& isset($_GET['location'] 
        var_dump($_GET);
        echo $id = intval($_GET['id']);
        echo $loc = $_GET['location'];
        $fileGetSql = "DELETE FROM `task_file_map` WHERE id = '$id'";
        $resultFileNameResult = mysql_query($fileGetSql);
        //$totalLocation stores the total path including the file .
        $totalLocation = "C:/wamp/www/phpproject/" . $loc;
        //$location strips the total location to get the current location of the file .
        $location = substr($totalLocation, 0, 33);
        //$fileName stores the name of the file
        echo "<br/> Delete the file :: " . $fileName = substr($totalLocation, 34);
        $old = getcwd(); // Save the current directory
        chdir($location);
        unlink($fileName);
        chdir($old); // Restore the old working directory 
        if (false === $resultFileNameResult) {
            echo mysql_error();
        }
        echo 'ok';
    }
    ?>
</html>