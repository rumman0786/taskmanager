function checkCredentials()
{
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    var name = document.getElementById('name');
    var email = document.getElementById('email');
    
    var emailMessage = document.getElementById('emailMessage');
    var nameMessage = document.getElementById('nameMessage');
    var message = document.getElementById('confirmMessage');
    
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    
    var buttonStatusPass = true ; // true denotes disabled
    var buttonStatusEmail = true ; // true denotes disabled
    var buttonStatusName = true ; // true denotes disabled
    
    if( name.value != '' && (/^[a-zA-Z ]*$/.test(name.value))){
        buttonStatusName = false ;
        name.style.backgroundColor = goodColor;
        nameMessage.style.color = goodColor;
        nameMessage.innerHTML = "Correct Name"
    }else{
        buttonStatusName = true ;
        nameMessage.style.color = badColor;
        nameMessage.innerHTML = "Name cannot be blank & can contain only letters and white space!"
    }
    
    if( (pass1.value == pass2.value) && (pass1.value != '' && pass2.value != '')){
        buttonStatusPass = false ;
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    }else{
        buttonStatusPass = true ;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
    
    if((/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(email.value))){
        buttonStatusEmail = false ;
        $('button').prop('disabled', false);
        emailMessage.style.color = goodColor;
        emailMessage.innerHTML = "Valid Email!";
    }else{
        buttonStatusEmail = true ;
        pass2.style.backgroundColor = badColor;
        emailMessage.style.color = badColor;
        emailMessage.innerHTML = "Invalid Email!";
    }
    
    var bool = (buttonStatusPass || buttonStatusEmail || buttonStatusName); // if either variable is true then one of the inputs must be invalid
    
    if(!bool){
        $('#registerButton').removeClass('disabled');    
    }else{
        $('#registerButton').addClass('disabled');    
    }
    return false ;
}  