function validateTask() {
    //    var name = document.forms["loginForm"]["taskName[]"].value;
    //    var description = document.forms["loginForm"]["description[]"].value;
    var form = document.forms[0];
    for (i = 0; i < form.elements.length; i++) {
        if (form.elements[i].type == "text" && form.elements[i].value == ""){
            alert("Name & Description must be filled out");
            return false;
        }
    }
    return true;
}


function validateFileSize() {
    
    var form = document.forms[0];
    for (i = 0; i < form.elements.length; i++) {
        if (form.elements[i].type == "file"){
            var file = form.elements[i].value;
            var fileSizeMB = document.getElementByClass('myFile').files[0].size/(1024*1024);
            alert(fileSizeMB);
            var fileName = file.substring(12);
            var extension = fileName.substring(fileName.lastIndexOf(".")+1);
            alert(extension);
            var allowedTypes = ['gif', 'png', 'jpg', 'jpeg'] ;
            var maxfilesizeInMB = 0.5;
            if( fileSizeMB > maxfilesizeInMB){
                alert("Cant upload more than 0.5MB\nYour File size is ::" +fileSizeMB.toFixed(2) +"MB"); 
                file.elements[i].value = "";
                file.elements[i].focus();
                return false ; 
            }
            if (allowedTypes.indexOf(extension) < 0) {
                alert(extension + " files are not allowed\nYou can only upload images");
                file.elements[i].value = "";
                file.elements[i].focus();
                return false ;
            }
        }
    }
    
    //	var file = document.getElementById('myFile');
    //	var fileSizeMB = (file.files[0].size)/(1024*1024) ;
    //	var fileName = file.files[0].name ;
    //	var maxfilesizeInMB = 0.5;
    //	var extension = fileName.substring(fileName.lastIndexOf(".")+1);
    //	var allowedTypes = ['gif', 'png', 'jpg', 'jpeg'] ;
    //
    //	if( fileSizeMB > maxfilesizeInMB){
    //		alert("Cant upload more than 0.5MB\nYour File size is ::" +fileSizeMB.toFixed(2) +"MB"); 
    //		    file.value = "";
    //            file.focus();
    //		return false ; 
    //	}
    //	if (allowedTypes.indexOf(extension) < 0) {
    //		alert(extension + " files are not allowed\nYou can only upload images");
    //		 file.value = "";
    //		 file.focus();
    //		return false ;
    //		}
    return true;
}
