$(document).ready(function(){

    $("#myFile").change(function(){
	var fileName =  $('input[type=file]').val().replace("C:\\fakepath\\" , '');
        console.log(fileName);

        var allowedTypes = ['gif', 'png', 'jpg', 'jpeg'] ;
        var extension = fileName.substring(fileName.lastIndexOf(".")+1);
        console.log(extension);

        var fileSizeMB = ($("#myFile")[0].files[0].size /(1024*1024)).toFixed(2);
        var maxfilesizeInMB = 0.5;
        $("#lblSize").html( fileSizeMB  + "MB"); 	

        if( fileSizeMB > maxfilesizeInMB){
            $("#lblSize").html( fileSizeMB  + "MB" + "\nToo Big"); 
            $("#myFile").val(""); //file.value = "";
            $("#myFile").focus();
        }

        if($.inArray(extension,allowedTypes) < 0 ){
            $("#lblSize").html( extension + " type file is not allowed."); 
            $("#myFile").val(""); //file.value = "";
            $("#myFile").focus();	
        }
    });

    $("#addButton").on("click", function() {
        var formTaskName = document.createElement("INPUT");
        formTaskName.setAttribute("type", "text");
        formTaskName.setAttribute("name", "taskName[]");
        formTaskName.setAttribute("class", "form-control");
        formTaskName.setAttribute("placeholder", "Task Name");
        $( ".innerForm" ).append( formTaskName);
        $( ".innerForm" ).append( "</br>");
        
        var formTaskDescription = document.createElement("INPUT");
        formTaskDescription.setAttribute("type", "text");
        formTaskDescription.setAttribute("name", "description[]");
        formTaskDescription.setAttribute("class", "form-control");
        formTaskDescription.setAttribute("placeholder", "Task Description");
        $( ".innerForm" ).append( formTaskDescription);
        $( ".innerForm" ).append( "</br>");
        
        var formFileInput = document.createElement("INPUT");
        formFileInput.setAttribute("id", "myFile");
        formFileInput.setAttribute("type", "file");
        formFileInput.setAttribute("name", "uploadedFile[]");
        formFileInput.setAttribute("class", "form-control");
        formFileInput.setAttribute("placeholder", "Task Description");
        $( ".innerForm" ).append( formFileInput);
        $( ".innerForm" ).append( "</br>");
        
        var formLabel = document.createElement("LABEL");
        formLabel.setAttribute("id", "#lblSize");
        var textLabel = document.createTextNode("File Size is :");
        formLabel.appendChild(textLabel);
        $( ".innerForm" ).append( formLabel);
        $( ".innerForm" ).append( "</br>");
        
        var formAddButton = document.createElement("BUTTON");
        formAddButton.setAttribute("id", "addButton")
        var textAdd = document.createTextNode("Add");
        formAddButton.appendChild(textAdd);
        $( ".innerForm" ).append( formAddButton);
        
        var formRemoveButton = document.createElement("BUTTON");
        var textRemove = document.createTextNode("Remove");
        formRemoveButton.appendChild(textRemove);
        $( ".innerForm" ).append( formRemoveButton);
        $( ".innerForm" ).append( "</br>");
                
        
    });


});
