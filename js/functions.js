function validateForm() {
    var x = document.forms["myForm"]["fname"].value;
    if (x==null || x=="") {
        alert("First name must be filled out");
        return false;
    }
}

function validateTask() {
    var name = document.forms["loginForm"]["taskName"].value;
    var description = document.forms["loginForm"]["description"].value;

    if (name==null || name=="") {
        alert("Name must be filled out");
        return false;
    }
    if (description==null || description=="") {
        alert("Description must be filled out");
        return false;
    }

}


function validateFileSize() {
	var file = document.getElementById('myFile');
	var fileSizeMB = (file.files[0].size)/(1024*1024) ;
	var fileName = file.files[0].name ;
	var maxfilesizeInMB = 0.5;
	var extension = fileName.substring(fileName.lastIndexOf(".")+1);
	var allowedTypes = ['gif', 'png', 'jpg', 'jpeg'] ;

	if( fileSizeMB > maxfilesizeInMB){
		alert("Cant upload more than 0.5MB\nYour File size is ::" +fileSizeMB.toFixed(2) +"MB"); 
		    file.value = "";
            file.focus();
		return false ; 
	}
	if (allowedTypes.indexOf(extension) < 0) {
		alert(extension + " files are not allowed\nYou can only upload images");
		 file.value = "";
		 file.focus();
		return false ;
		}

}
